# MyMovieApp

MyMovieApp is an iOS Application that gives you information about the latest movies and TV shows available today.

## Library Used
1. SwiftUI
2. CachedAsyncImage  - [github](https://github.com/lorenzofiamingo/swiftui-cached-async-image)

## Caching Mechanism Used
1. URLCache
2. CachedAsyncImage

I used the following caching mechanism as my data storage because it is what I using in my projects to cater offline data. URLCache by default is already saving all the response we receive and it is just kept inside our iPhone, by using a correct URLSessionConfiguration your urlsession will give the last saved data requested when you still have active connections.

 It is also handy to use urlcache with Etag.

```Swift
let configuration = URLSessionConfiguration.default
configuration.httpCookieAcceptPolicy = .always
configuration.urlCache?.memoryCapacity = 50_000_000
configuration.urlCache?.diskCapacity = 100_000_000
return configuration
```

## Movie API Used
1. Started the project with OMDB API but with limitations to searching of release dates and other details.
2. Currently I am using TMDB API because I like how the search criteria is offered.

