import SwiftUI

struct MovieListView: View {
    @StateObject private var viewModel = MovieSeriesViewModel()
    @State private var searchText = ""
    @State private var showSearchFilter = false
    @State private var selectedSegment = 0
    
    var body: some View {
        NavigationStack {
            ScrollView {
                VStack(spacing: 0) {
                    if showSearchFilter {
                        SearchBar(text: $searchText)
                        
                        Picker("Options", selection: $selectedSegment) {
                            Text("Movie").tag(0)
                            Text("TV").tag(1)
                        }
                        .pickerStyle(SegmentedPickerStyle())
                        .padding()
                    }
                    
                    if viewModel.isLoading {
                        ActivityIndicator()
                    } else {
                        ForEach(viewModel.movies, id: \.id) { movie in
                            let img = BaseURLs.imgURL + (movie.poster ?? "")
                            let title = movie.original_title
                            let overview = movie.overview
                            let dateTime = movie.release_date ?? movie.airDate ?? Date().getTodayString()
                            let votes = movie.votes
                            NavigationLink(destination: DetailView(viewModel: viewModel,
                                                                   id: movie.id,
                                                                   imageURL: img,
                                                                   title: title ?? "",
                                                                   overview: overview,
                                                                   dateTime: dateTime.formatDate(),
                                                                   votes: votes,
                                                                   searchType: .movie), label: {
                                ListCellView(imageURL: img,
                                             title: title ?? "",
                                             overview: overview,
                                             dateTime: dateTime.formatDate(),
                                             votes: votes)
                                .padding(3)
                            })
                           
                            Divider()
                        }
                    }
                }
                .frame(maxWidth: .infinity)
                .padding()
            }
            .background(Colors.background)
            .navigationTitle("Latest Movies")
            .toolbar {
                Button(action: {
                    self.showSearchFilter.toggle()
                }) {
                    Image(systemName: "magnifyingglass")
                        .imageScale(.large)
                        .foregroundColor(.white)
                }
            }
        }
        .onChange(of: searchText, perform: { value in
            Task {
                if !value.isEmpty && value.count > 3 {
                    try? await viewModel.searchTitle(searchString: searchText, searchType: selectedSegment == 0 ? .movie : .series)
                }
            }
        })
        .onAppear() {
            Task {
                try? await viewModel.getTopMoviesOfTheYear()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MovieListView()
    }
}

struct SearchBar: View {
    @Binding var text: String

    var body: some View {
        HStack {
            TextField("Search", text: $text)
                .frame(maxWidth: .infinity)
                .frame(height: 40)
                .background(Color(.systemGray6))
                .cornerRadius(8)
                .padding(.leading, 8)

            Button(action: {
                self.text = ""
            }) {
                Image(systemName: "xmark.circle.fill")
                    .foregroundColor(.white)
            }
            .padding(.trailing, 8)
        }
    }
}
