import Foundation

struct MovieDetailResponse: Codable {
    let movieDetail: MovieDetail
    
    private enum CodingKeys: String, CodingKey {
        case movieDetail = "results"
    }
}

struct MovieDetail: Codable {
    let id: Int
    let homepage: String
    let title: String?
    let name: String?
    let tagline: String
    let genres: [Genres]
}

struct Genres: Codable {
    let id: Int
    let name: String
}
