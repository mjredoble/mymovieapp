import Foundation

struct MovieResponse: Codable {
    let movies: [Movie]
    
    private enum CodingKeys: String, CodingKey {
        case movies = "results"
    }
}

struct Movie: Codable {
    let id: Int
    let original_title: String?
    let release_date: String?
    let poster: String?
    let overview: String
    let votes: Double
    let airDate: String?
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case original_title = "original_title"
        case release_date = "release_date"
        case poster = "poster_path"
        case overview = "overview"
        case votes = "vote_average"
        case airDate = "first_air_date"
    }
}
