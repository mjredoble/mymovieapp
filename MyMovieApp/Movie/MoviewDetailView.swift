import SwiftUI
import CachedAsyncImage

struct DetailView: View {
    var imageURL: String
    var title: String
    var overview: String
    var dateTime: String
    var votes: Double
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        NavigationStack {
            ScrollView {
                VStack(alignment: .center) {
                    ZStack(alignment: .topLeading) {
                        CachedAsyncImage(url: URL(string: imageURL), content: { image in
                            image.resizable()
                                .mask(LinearGradient(gradient: Gradient(stops: [
                                           .init(color: .clear, location: 0),
                                           .init(color: .black, location: 0.25),
                                           .init(color: .black, location: 0.75),
                                           .init(color: .clear, location: 1)
                                       ]), startPoint: .top, endPoint: .bottom))
                                .frame(height: 450)
                                .frame(maxWidth: .infinity)
                                .padding(.top, 10)
                        }, placeholder: {
                            Image("placeholder")
                                .resizable()
                                .renderingMode(.original)
                                .mask(LinearGradient(gradient: Gradient(stops: [
                                           .init(color: .clear, location: 0),
                                           .init(color: .black, location: 0.25),
                                           .init(color: .black, location: 0.75),
                                           .init(color: .clear, location: 1)
                                       ]), startPoint: .top, endPoint: .bottom))
                                .frame(height: 400)
                                .frame(maxWidth: .infinity)
                                .padding(.top, 10)
                        })
                        
                        Button(action: {
                            self.presentationMode.wrappedValue.dismiss()
                        }) {
                            Image(systemName: "arrow.left.circle.fill")
                                .resizable()
                                .frame(width: 40, height: 40)
                                .foregroundColor(.white)
                                .padding(.leading, 20)
                                .padding(.top, 60)
                        }
                    }
                    
                    HStack {
                        Text(title)
                            .bold()
                            .font(.title)
                            .foregroundColor(.white)
                        
                        Spacer()
                        Image(systemName: "star.fill")
                            .foregroundColor(.yellow)
                            .frame(maxWidth: 5)
                            .padding(2)
                        Text(String(format: "%.1f", votes))
                            .font(.body)
                            .fontWeight(.bold)
                            .foregroundColor(.yellow)
                            .padding(.vertical, 5)
                    }
                    .padding(.horizontal, 30)
                    
                    Text(overview)
                        .padding(.horizontal, 30)
                        .padding(.top, 3)
                        .foregroundColor(.white)
                        .font(.body)
                    
                    Spacer()
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            }
            .navigationBarHidden(true)
            .ignoresSafeArea()
            .background(Colors.background)
        }
        
    }
    
    struct StarRatingView: View {
        var rating: Double
        var maxRating: Double

        var body: some View {
            HStack {
                ForEach(0..<Int(maxRating), id: \.self) { index in
                    Image(systemName: index < Int(rating) ? "star.fill" : "star")
                        .foregroundColor(.yellow)
                        .frame(maxWidth: 5)
                        .padding(2)
                }
            }
        }
    }

}

#Preview {
    DetailView(imageURL: "", title: "", overview: "", dateTime: "", votes: 2.0)
}
