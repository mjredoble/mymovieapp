import SwiftUI

struct MonthlySeriesView: View {
    @StateObject private var viewModel = MovieSeriesViewModel()

    var body: some View {
        NavigationStack {
            ScrollView {
                VStack(spacing: 0) {
                    ForEach(viewModel.series, id: \.id) { series in
                        ListCellView(imageURL: BaseURLs.imgURL + series.poster,
                                   title: series.title,
                                   overview: series.overview,
                                   dateTime: series.airDate.formatDate(),
                                   votes: series.votes)
                            .padding(3)
                        Divider()
                    }
                }
                .frame(maxWidth: .infinity)
                .padding()
            }
            .background(Colors.background)
            .navigationTitle("New This Month")
        }
        .onAppear() {
            Task {
                try? await viewModel.getTrendingThisMonth()
            }
        }
    }
}

struct MonthlySeriesView_Previews: PreviewProvider {
    static var previews: some View {
        MonthlySeriesView()
    }
}
