import Foundation

struct TvResponse: Codable {
    let tvSeries: [Series]
    
    private enum CodingKeys: String, CodingKey {
        case tvSeries = "results"
    }
}

struct Series: Codable {
    let id: Int
    let title: String
    let poster: String
    let overview: String
    let airDate: String
    let votes: Double
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "original_name"
        case poster = "poster_path"
        case overview = "overview"
        case airDate = "first_air_date"
        case votes = "vote_average"
    }
}
