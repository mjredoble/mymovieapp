import SwiftUI

struct SeriesListVIew: View {
    @StateObject private var viewModel = MovieSeriesViewModel()

    var body: some View {
        NavigationStack {
            ScrollView {
                VStack(spacing: 0) {
                    ForEach(viewModel.series, id: \.id) { movie in
                        let img = BaseURLs.imgURL + movie.poster
                        let title = movie.title
                        let overview = movie.overview
                        let dateTime = movie.airDate
                        let votes = movie.votes
                        NavigationLink(destination: DetailView(viewModel: viewModel,
                                                               id: movie.id,
                                                               imageURL: img,
                                                               title: title,
                                                               overview: overview,
                                                               dateTime: dateTime,
                                                               votes: votes,
                                                               searchType: .series), label: {
                            ListCellView(imageURL: img,
                                       title: title,
                                       overview: overview,
                                       dateTime: dateTime,
                                       votes: votes)
                            .padding(3)
                        })
                       
                        Divider()
                    }
                    
                    ForEach(viewModel.series, id: \.id) { series in
                        ListCellView(imageURL: BaseURLs.imgURL + series.poster,
                                   title: series.title,
                                   overview: series.overview,
                                   dateTime: series.airDate,
                                   votes: series.votes)
                        .padding(3)
                        Divider()
                    }
                }
                .frame(maxWidth: .infinity)
                .padding()
            }
            .background(Colors.background)
            .navigationTitle("Series")
        }
        .onAppear() {
            Task {
                try? await viewModel.getTopSeriesOfTheYear()
            }
        }
    }
}

struct SeriesListVIew_Previews: PreviewProvider {
    static var previews: some View {
        SeriesListVIew()
    }
}
