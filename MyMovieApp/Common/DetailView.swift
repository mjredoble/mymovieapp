import SwiftUI
import CachedAsyncImage

struct DetailView: View {
    @StateObject var viewModel: MovieSeriesViewModel
    
    var id: Int
    var imageURL: String
    var title: String
    var overview: String
    var dateTime: String
    var votes: Double
    var searchType: SearchType
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        NavigationStack {
            ScrollView {
                VStack(alignment: .leading) {
                    ZStack(alignment: .topLeading) {
                        CachedAsyncImage(url: URL(string: imageURL), content: { image in
                            image.resizable()
                                .mask(LinearGradient(gradient: Gradient(stops: [
                                    .init(color: .clear, location: 0),
                                    .init(color: .black, location: 0.25),
                                    .init(color: .black, location: 0.75),
                                    .init(color: .clear, location: 1)
                                ]), startPoint: .top, endPoint: .bottom))
                                .frame(height: 500)
                                .frame(maxWidth: .infinity)
                                .padding(.top, 10)
                        }, placeholder: {
                            Image("placeholder")
                                .resizable()
                                .renderingMode(.original)
                                .mask(LinearGradient(gradient: Gradient(stops: [
                                    .init(color: .clear, location: 0),
                                    .init(color: .black, location: 0.25),
                                    .init(color: .black, location: 0.85),
                                    .init(color: .clear, location: 1)
                                ]), startPoint: .top, endPoint: .bottom))
                                .frame(height: 400)
                                .frame(maxWidth: .infinity)
                                .padding(.top, 10)
                        })
                        
                        Button(action: {
                            self.presentationMode.wrappedValue.dismiss()
                        }) {
                            Image(systemName: "arrow.left.circle.fill")
                                .resizable()
                                .frame(width: 50, height: 50)
                                .foregroundColor(.white)
                                .padding(.leading, 20)
                                .padding(.top, 50)
                        }
                    }
                    
                    HStack {
                        Text(title)
                            .font(.system(size: 25))
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                        
                        Spacer()
                        Image(systemName: "star.fill")
                            .foregroundColor(.yellow)
                            .frame(maxWidth: 10)
                        Text(String(format: "%.1f", votes))
                            .font(.title3)
                            .fontWeight(.bold)
                            .foregroundColor(.yellow)
                    }
                    .padding(.horizontal, 30)
                    .padding(.bottom, 10)
                    
                    //TODO: Some Details
                    if let detail = viewModel.selectedMovieDetail {
                        if !detail.tagline.isEmpty {
                            Text("\"\(detail.tagline)\"")
                                .font(.system(size: 20))
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                                .frame(maxWidth: .infinity)
                                .multilineTextAlignment(.center)
                                .padding(10)
                        }
                        
                        if let url = URL(string: detail.homepage) {
                            Text("Website")
                                .foregroundColor(.white)
                                .frame(maxWidth: .infinity)
                                .multilineTextAlignment(.center)
                            Link("\(detail.homepage)", destination: url)
                                .font(.headline)
                                .foregroundColor(.blue)
                                .frame(maxWidth: .infinity)
                                .multilineTextAlignment(.center)
                                .padding([.horizontal])
                        }
                        
                        
                        HStack(alignment: .center) {
                            ForEach(detail.genres, id: \.id) { genre in
                                Text(genre.name)
                                    .font(.system(size: 12))
                                    .foregroundColor(.white)
                                    .padding(8)
                                    .background(Color.blue)
                                    .cornerRadius(10)
                            }
                        }
                        .frame(maxWidth: .infinity)
                        .padding(.horizontal, 15)
                    }
                    //
                    
                    Text(overview)
                        .padding(.horizontal, 20)
                        .padding(.top, 3)
                        .foregroundColor(.white)
                        .font(.body)
                    
                    Spacer()
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .padding(.bottom, 150)
            }
            .navigationBarHidden(true)
            .ignoresSafeArea()
            .background(Colors.background)
        }
        .onAppear() {
            Task {
                do {
                    try await viewModel.getMovieDetail(movieId: id, searchType: searchType)
                } catch {
                    //TODO: Show error message
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
        }
        
    }
}

//#Preview {
//    DetailView(viewModel: MovieViewModel(), id: 1, imageURL: "", title: "Hello World", overview: "Test ", dateTime: "2023-09-12", votes: 2.0, searchType: .series)
//}
