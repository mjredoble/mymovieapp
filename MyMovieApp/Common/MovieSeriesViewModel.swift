import Foundation

@MainActor
final class MovieSeriesViewModel: ObservableObject  {
    @Published var movies = [Movie]()
    @Published var selectedMovieDetail: MovieDetail?
    @Published var series = [Series]()
    @Published var isLoading = false
    
    private var defaultSearch = ""
    
    func getTopMoviesOfTheYear() async throws {
        var components = UrlUtils.tmdbURLComponents(searchType: .movie)
        components.queryItems?.append(contentsOf: [
            URLQueryItem(name: "primary_release_year", value: "2023"),
            URLQueryItem(name: "sort_by", value: "popularity.desc"),
            URLQueryItem(name: "page", value: "1")
        ])
        
        guard let url = components.url else {
            throw ServiceError.badUrl
        }
        
        do {
            let result = try await HTTPClient().request(url: url, method: .GET)
            let movieResponse = try JSONDecoder().decode(MovieResponse.self, from: result.body)
            
            movies.removeAll()
            self.movies.append(contentsOf: movieResponse.movies)
        } catch {
            throw ServiceError.badResponse
        }
    }
    
    func getMovieDetail(movieId: Int, searchType: SearchType) async throws {
        let baseURL = "https://api.themoviedb.org/3/" + searchType.rawValue + "/\(movieId)"
        var components = URLComponents(string: baseURL)
        components!.queryItems = [
            URLQueryItem(name: "api_key", value: ApiKeys.tmdbkey),
        ]
        
        guard let url = components?.url else {
            throw ServiceError.badUrl
        }
        
        do {
            let result = try await HTTPClient().request(url: url, method: .GET)
            let detail = try JSONDecoder().decode(MovieDetail.self, from: result.body)
            self.selectedMovieDetail = detail
        } catch {
            throw ServiceError.badResponse
        }
    }
    
    // MARK: - Tv Series
    func getTopSeriesOfTheYear() async throws {
        var components = UrlUtils.tmdbURLComponents(searchType: .series)
        components.queryItems?.append(contentsOf: [
            URLQueryItem(name: "primary_release_year", value: "2023"),
            URLQueryItem(name: "sort_by", value: "popularity.desc"),
            URLQueryItem(name: "page", value: "1")
        ])
        
        guard let url = components.url else {
            throw ServiceError.badUrl
        }
        
        do {
            let result = try await HTTPClient().request(url: url, method: .GET)
            let movieResponse = try JSONDecoder().decode(TvResponse.self, from: result.body)
            
            series.removeAll()
            self.series.append(contentsOf: movieResponse.tvSeries)
        } catch {
            throw ServiceError.badResponse
        }
    }
    
    func getTrendingThisMonth() async throws {
        let start = Date().startOfMonth()
        let end = Date().endOfMonth()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        var components = UrlUtils.tmdbURLComponents(searchType: .series)
        components.queryItems?.append(contentsOf: [
            URLQueryItem(name: "air_date.gte", value: dateFormatter.string(from: start)),
            URLQueryItem(name: "air_date.lte", value: dateFormatter.string(from: end)),
            URLQueryItem(name: "page", value: "1")
        ])
        
        guard let url = components.url else {
            throw ServiceError.badUrl
        }
        
        do {
            let result = try await HTTPClient().request(url: url, method: .GET)
            let movieResponse = try JSONDecoder().decode(TvResponse.self, from: result.body)
            
            series.removeAll()
            self.series.append(contentsOf: movieResponse.tvSeries)
        } catch {
            throw ServiceError.badResponse
        }
    }
    
    func searchTitle(searchString: String, searchType: SearchType) async throws {
        let baseURL = "https://api.themoviedb.org/3/search/" + searchType.rawValue
        var components = URLComponents(string: baseURL)
        components!.queryItems = [
            URLQueryItem(name: "api_key", value: ApiKeys.tmdbkey),
            URLQueryItem(name: "query", value: searchString),
        ]
        
        guard let url = components?.url else {
            isLoading = false
            throw ServiceError.badUrl
        }
        
        do {
            let result = try await HTTPClient().request(url: url, method: .GET)
            let movieResponse = try JSONDecoder().decode(MovieResponse.self, from: result.body)
            movies.removeAll()
            self.movies.append(contentsOf: movieResponse.movies)
        } catch {
            throw ServiceError.badResponse
        }
    }
}
