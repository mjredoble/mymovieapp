import SwiftUI
import CachedAsyncImage

struct ListCellView: View {
    var imageURL: String
    var title: String
    var overview: String
    var dateTime: String
    var votes: Double
    
    var body: some View {
        HStack(alignment: .top, spacing: 0) {
            CachedAsyncImage(url: URL(string: imageURL), content: { image in
                image.resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: 130)
                    .padding(.top, 10)
            }, placeholder: {
                Rectangle()
                    .fill(Color.gray.opacity(0.5))
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 130, height: 200)
                    .redacted(reason: .placeholder)
            })
                        
            VStack(alignment: .leading, spacing: 0) {
                Text(title)
                    .bold()
                    .multilineTextAlignment(.leading)
                    .foregroundColor(.white)
                    .padding(.top, 10)
                Text(overview)
                    .font(.footnote)
                    .multilineTextAlignment(.leading)
                    .foregroundColor(.white)
                    .padding(.vertical, 10)
                
                HStack{
                    Image(systemName: "star.fill")
                        .resizable()
                        .foregroundColor(.yellow)
                        .frame(width: 15, height: 15)
                    Text(String(format: "%.1f", votes))
                        .font(.footnote)
                        .fontWeight(.bold)
                        .foregroundColor(.yellow)
                        .padding(.top, 5)
                    Spacer()
                    Text(dateTime)
                        .font(.footnote)
                        .foregroundColor(.white)
                }
                .padding(.vertical, 10)
            }
            .padding(.leading, 15)
        }
    }
}
