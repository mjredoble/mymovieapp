import SwiftUI

struct MainView: View {
    
    init() {
        let navigationBarAppearance = UINavigationBarAppearance()
            navigationBarAppearance.configureWithOpaqueBackground()
            navigationBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navigationBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navigationBarAppearance.backgroundColor = UIColor(Colors.background)
        
            UINavigationBar.appearance().standardAppearance = navigationBarAppearance
            UINavigationBar.appearance().compactAppearance = navigationBarAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = navigationBarAppearance
            UITabBar.appearance().barTintColor = UIColor(Colors.background)
    }
    
    var body: some View {
        TabView {
            MovieListView()
                .tabItem {
                    Image(systemName: "ticket")
                    Text("Movies")
                }
            
            SeriesListVIew()
                .tabItem {
                    Image(systemName: "tv")
                    Text("Series")
                }
            
            MonthlySeriesView()
                .tabItem {
                    Image(systemName: "play.tv")
                    Text("Episodes")
                }
        }
    }
}

