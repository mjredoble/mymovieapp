import Foundation

enum HTTPMethod: String {
    case GET
    case POST
    case PUT
    case DELETE
    
    init(_ method: String) {
        switch method {
        case "GET":
            self = HTTPMethod.GET
        case "POST":
            self = HTTPMethod.POST
        case "PUT":
            self = HTTPMethod.PUT
        case "DELETE":
            self = HTTPMethod.DELETE
        default:
            self = HTTPMethod.GET
        }
    }
}

struct HTTPClientResult {
    let statusCode: Int
    let headers: [AnyHashable: Any]
    let body: Data
}

enum HTTPClientError: Error {
    case applicationLevel(statusCode: Int, body: Data?)
    case lowerLevel(_ error: Error)
}

final class HTTPClient {
    var baseURL = ""
    var timeout = 0
    
    func request(url: URL, method: HTTPMethod, body: Data? = nil) async throws -> HTTPClientResult {
        var request = URLRequest(url: url)
        request.timeoutInterval = TimeInterval(timeout)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = body
        
        let session = URLSession(configuration: getConfiguration())
        
        do {
            let (data, urlResponse) = try await session.data(for: request)
            
            let response = urlResponse as! HTTPURLResponse
            let str = String(decoding: data, as: UTF8.self)
            
            switch response.statusCode {
            case  200...204:
                print("🟢 API: \(url.absoluteString) --- DATA:\(data) ---STATUS:\(response.statusCode)")
                return HTTPClientResult(statusCode: response.statusCode, headers: response.allHeaderFields, body: data)
            default:
                print("🔴 API: \(url.absoluteString) --- DATA:\(str) ---STATUS:\(response.statusCode)")
                throw HTTPClientError.applicationLevel(statusCode: response.statusCode, body: data)
            }
        } catch {
            throw HTTPClientError.lowerLevel(error)
        }
    }
    
    func getConfiguration() -> URLSessionConfiguration {
        let configuration = URLSessionConfiguration.default
        configuration.httpCookieAcceptPolicy = .always
        configuration.urlCache?.memoryCapacity = 50_000_000
        configuration.urlCache?.diskCapacity = 100_000_000
        return configuration
    }
}
