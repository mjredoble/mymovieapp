import Foundation
import SwiftUI

struct ApiKeys {
    static let omdbkey = "6a7655e9"
    static let tmdbkey = "de292efea199c85be96624c23ef9eeb2"
}

struct BaseURLs {
    static let imgURL = "https://image.tmdb.org/t/p/original"
}

enum ServiceError: Error {
    case badUrl
    case badResponse
}

enum SearchType: String {
    case movie = "movie"
    case series = "tv"
 }

struct UrlUtils {
    static func tmdbURLComponents(searchType: SearchType) -> URLComponents {
        let baseURL = "https://api.themoviedb.org/3/discover/" + searchType.rawValue
        var components = URLComponents(string: baseURL)
        components!.queryItems = [
            URLQueryItem(name: "api_key", value: ApiKeys.tmdbkey),
        ]
        
        return components!
    }
}

struct Colors {
    static let background = Color(red: 40 / 255, green: 44 / 255, blue: 52 / 255)
    static let navbarBackground = UIColor(red: 45/255, green: 52/255, blue:54/255, alpha: 1)
    static let gradient = LinearGradient(colors: [Color(red: 40 / 255, green: 44 / 255, blue: 52 / 255), Color(red: 81 / 255, green: 85 / 255, blue: 107 / 255)],
                                  startPoint: .top, endPoint: .bottom)
}

