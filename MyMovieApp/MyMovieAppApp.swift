import SwiftUI

@main
struct MyMovieAppApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
